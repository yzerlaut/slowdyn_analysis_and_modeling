from neo.io import AxonIO
import numpy as np
import os, sys
import matplotlib.pylab as plt
from graphs.my_graph import set_plot
from os.path import expanduser

old_facq = 50. #kHz
new_facq = 1 #kHz
TAU = 20e-3
threshold = -52.03 # determined from get_hist.py
NTRACE = 1

    
filename = expanduser("~")+'/files/DATA/Data_Ste_Zucca/Cell1_13909028.abf'
data1 = AxonIO(filename).read_block()
vec = []
for i in range(NTRACE):
    vec1 = np.array(data1.segments[i].analogsignals[0])
    vec = np.concatenate([vec, vec1])
    
dwnsample_data = vec.reshape(-1, int(old_facq/new_facq)).mean(axis=1)
dwnsample_data[dwnsample_data>-30] = -30

fig, ax = plt.subplots(figsize=(6,3))
plt.subplots_adjust(left=.2, bottom=.25)
if not len(sys.argv)>1:
    ax.plot(np.arange(len(vec))/old_facq/1e3, vec, color='gray', label='raw data (50kHz)')
    
def filter(vec, tau=5e-3, dt=1e-3/new_facq, spike_threshold=-40):
    vec[vec>spike_threshold] = spike_threshold
    n_points = int(3*tau/dt)
    decay_exp = np.exp(-dt/tau*np.arange(n_points))
    decay_exp /= decay_exp.mean()
    return np.array([np.mean(vec[i-n_points:i]*decay_exp) if i>n_points else vec[i] for i in range(len(vec))])

def get_state_timings(t, vec, threshold=threshold, tau=TAU):
    i_upward  = np.argwhere((vec[1:]>threshold) & (vec[:-1]<=threshold)) # upward crossings
    i_downward  = np.argwhere((vec[1:]<threshold) & (vec[:-1]>=threshold)) # downward crossings
    t_upward, t_downward  = t[i_upward], t[i_downward]
    DOWN_STATE, UP_STATE = [], []
    iupward, idownward = 0, 0
    if t_upward[0]<t_downward[0]:
        DOWN_STATE.append([0,t_upward[0][0]])
        iupward +=1
    else:
        UP_STATE.append([0,t_downward[0][0]])
        idownward +=1
    while((iupward<len(i_upward)) and (idownward<len(i_downward))):
        DOWN_STATE.append([t_downward[idownward][0],t_upward[iupward][0]])
        UP_STATE.append([t_upward[iupward][0],t_downward[idownward][0]])
        idownward +=1
        iupward +=1
    return DOWN_STATE, UP_STATE
DOWN_STATE, UP_STATE = get_state_timings(\
                    1e-3*np.arange(len(dwnsample_data))/new_facq,\
                        filter(dwnsample_data, tau=TAU), threshold=threshold)

ax.plot(np.arange(len(dwnsample_data))*1e-3/new_facq,\
        filter(dwnsample_data, tau=TAU), 'r-',\
        label=str(int(new_facq))+\
        'kHz sampling + spike removed \n +real-time low-pass, $\\tau$='+\
        str(int(1e3*TAU))+'ms', lw=2)
ax.plot([0,len(dwnsample_data)*1e-3/new_facq], [threshold, threshold], 'b-', lw=3, alpha=.3, label='threshold')
if not len(sys.argv)>1:
    plt.legend(frameon=False, prop={'size':'xx-small'})
for dwn in DOWN_STATE:
    ax.fill_between([dwn[0], dwn[1]],\
                    [vec.min(), vec.min()], [-30, -30],\
                    color='k', alpha=.1, lw=0)
set_plot(ax, xlabel='time (s)', ylabel='$V_m$ (mV)')
fig.savefig('fig.png')
