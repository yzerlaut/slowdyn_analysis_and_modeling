import sys, os, glob
import matplotlib
matplotlib.use('Qt5Agg') 
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from PyQt5 import QtGui, QtWidgets, QtCore
import numpy as np
import time
ROOT_PATH = os.path.expanduser('~')+os.path.sep # TO BE CHANGED IN SETUP !!
from sklearn import mixture

def get_last_bin_file(dir='./'):
    """ get lastly created bin file """
    FILES = glob.glob(dir+os.path.sep+'*.bin')
    FILES.sort(key=lambda x: os.path.getmtime(x))
    print(FILES)
    return FILES[-1]

def load_file(filename, zoom=100):
    dt = 1e-4 # acquisition frequency 10kHz
    data = np.fromfile(filename, dtype=np.float32)
    return data[-int(zoom/dt):]

def fit_gaussians(Vm, n=1000, ninit=3, bound1=-90, bound2=-35):
    # clf = mixture.GaussianMixture(n_components=2, max_iter=n, n_init=ninit, means_init=((-80,), (-50,)), covariance_type='spherical')
    clf = mixture.GaussianMixture(n_components=3, max_iter=n, n_init=ninit, means_init=((-80,), (-65,), (-50,)), covariance_type='spherical')
    clf.fit(np.array((Vm[(Vm>bound1) & (Vm<bound2)],)).T)
    return clf.weights_, clf.means_.flatten(), np.sqrt(clf.covariances_)

def determine_thresholds(weights, means, stds):
    """ Gives the thresholds given the Gaussian Mixture"""
    i0, i1 = np.argmin(means[0:2]), np.argmax(means[1:3])+1
    alpha = 1.-np.exp(-hvsd(means[i1]-2.*stds[i1]-means[i0]-2.*stds[i0])/5.)
    return means[i0]+2.*alpha*stds[i0], means[i1]-2.*alpha*stds[i1]

def hvsd(x):
    return .5*(1.+np.sign(x))*x

class Window(QtWidgets.QMainWindow):
    """ subclassing QtWidgets.QMainWindow """
    def __init__(self, parent=None):
        """ """
        super(Window, self).__init__(parent)
        # buttons and functions
        LABELS = ["q) Quit", "o) Open File", "f) Set Folder"]
        FUNCTIONS = [self.close_app, self.file_open, self.folder_open]
        button_length = 113.
        self.setWindowTitle('Calculating the Up and Down states thresholds')
        self.setGeometry(50, 50, button_length*(len(LABELS)), 90)

        mainMenu = self.menuBar()
        self.fileMenu = mainMenu.addMenu('&File')
        for func, label, hshift in zip(FUNCTIONS, LABELS, button_length*np.arange(len(LABELS))):
            btn = QtWidgets.QPushButton(label, self)
            btn.clicked.connect(func)
            btn.setMinimumWidth(button_length)
            btn.move(hshift, 0)
            action = QtWidgets.QAction(label, self)
            action.setShortcut(label.split(')')[0])
            action.triggered.connect(func)
            self.fileMenu.addAction(action)
            
        label, func = "c) ========  Calculate ========", self.analyze
        btn = QtWidgets.QPushButton(label, self)
        btn.clicked.connect(func)
        btn.setMinimumWidth(button_length*(len(LABELS)))
        btn.setMinimumHeight(40)
        btn.move(0, 30)
        action = QtWidgets.QAction(label, self)
        action.setShortcut(label.split(')')[0])
        action.triggered.connect(func)
        self.fileMenu.addAction(action)
        # default folder
        self.folder = ROOT_PATH+'DATA_elphy'+os.path.sep+time.strftime("%Y_")+\
                      str(int(time.strftime("%m")))+"_"+str(int(time.strftime("%d")))
        print(self.folder)
        if not os.path.exists(self.folder):
            self.folder = ROOT_PATH+'DATA_elphy'
        self.force_file = False # by default will be last generated file
        self.show()

    def analyze(self):
        if not self.force_file:
            self.filename = get_last_bin_file(self.folder)
        self.statusBar().showMessage('Analyzing Datafile: '+self.filename.split(os.path.sep)[-1])
        Vm = load_file(self.filename)
        threshold1, threshold2 = determine_thresholds(*fit_gaussians(Vm))
        ff = open(self.folder+os.path.sep+'thresholds.txt', 'w')
        ff.write(str(threshold1)+'\n'+str(threshold2))
        ff.close()
        self.statusBar().showMessage('Vth1='+str(int(threshold1))+', Vth2='+str(int(threshold2)))
        self.force_file = False
    
    def close_app(self):
        sys.exit()

    def file_open(self):
        name=QtWidgets.QFileDialog.getOpenFileName(self, 'Open File',\
                                                   self.folder)
        if name[0]!='':
            self.force_file = True
            self.filename = name[0]
            self.statusBar().showMessage('Datafile will be: '+name[0].split(os.path.sep)[-1])

    def folder_open(self):
        name=QtWidgets.QFileDialog.getExistingDirectory(self, 'Select Folder', self.folder)
        if name!='':
            self.folder = name

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main = Window()
    main.show()
    sys.exit(app.exec_())
