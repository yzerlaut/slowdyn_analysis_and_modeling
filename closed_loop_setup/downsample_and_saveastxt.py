from IO.files_manip import get_files_with_given_ext
from neo.io import AxonIO
import numpy as np
import os

old_facq = 50. #kHz
new_facq = 1. #kHz

for filename in get_files_with_given_ext(dir='./', ext='abf'):
    if not os.path.isfile(filename.replace('abf', 'txt')):
        data1 = AxonIO(filename).read_block()
        vec = []
        for i in range(len(data1.segments)):
            vec1 = np.array(data1.segments[i].analogsignals[0])
            vec = np.concatenate([vec, vec1])

        vec.reshape(-1, int(old_facq/new_facq)).mean(axis=1) 
        np.savetxt(filename.replace('abf', 'txt'), vec)
        print(filename)
