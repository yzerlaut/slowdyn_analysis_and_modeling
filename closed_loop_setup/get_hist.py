from neo.io import AxonIO
import numpy as np
import os, sys
import matplotlib.pylab as plt
from graphs.my_graph import set_plot
from os.path import expanduser

old_facq = 50. #kHz
new_facq = 1 #kHz

filename = expanduser("~")+'/files/DATA/Data_Ste_Zucca/Cell1_13909028.abf'
data1 = AxonIO(filename).read_block()
vec = []
for i in range(20):
    vec1 = np.array(data1.segments[i].analogsignals[0])
    vec = np.concatenate([vec, vec1])
    
dwnsample_data = vec.reshape(-1, int(old_facq/new_facq)).mean(axis=1)
dwnsample_data[dwnsample_data>-30] = -30

fig, ax = plt.subplots(figsize=(5,3))
plt.subplots_adjust(left=.2, bottom=.25)
hist, bins = np.histogram(dwnsample_data, bins=40, normed=True)
plt.plot(bins[2:], np.diff(hist))
plt.bar(.5*(bins[1:]+bins[:-1]), hist,\
        edgecolor='k', facecolor='lightgray', lw=2)
i0 = np.argmax(hist) # down state peak
deriv = np.diff(hist)
ii = i0
while not ((deriv[ii+1]>0) and (deriv[ii]<0)):
    ii+=1
i1 = ii+2
print(bins[i1])
plt.plot([bins[i1], bins[i1]], [0, hist.max()], 'b-', alpha=.5, lw=4)
set_plot(ax, xlabel='$V_m$ (mV)', ylabel='count (norm.)')
fig.savefig('hist.png')
