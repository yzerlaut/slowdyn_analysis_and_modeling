from neo.io import AxonIO
import numpy as np
import os, sys
import matplotlib.pylab as plt
from graphs.my_graph import set_plot
from os.path import expanduser

old_facq = 50. #kHz
new_facq = 0.1 #kHz

TAU = 20e-3

if len(sys.argv)>1:
    NTRACE = int(sys.argv[-1])
else:
    NTRACE = 1
filename = expanduser("~")+'/DATA/Data_Ste_Zucca/LFP1_25632003.abf'
data1 = AxonIO(filename).read_block()
vec = []
for i in range(NTRACE):
    vec1 = 1e3*np.array(data1.segments[i].analogsignals[0])
    vec = np.concatenate([vec, vec1])
    
dwnsample_data = vec.reshape(-1, int(old_facq/new_facq)).mean(axis=1)

fig, ax = plt.subplots(2, figsize=(6,6))
plt.subplots_adjust(left=.2, bottom=.25)
# if not len(sys.argv)>2:
#     ax[0].plot(np.arange(len(vec))/old_facq/1e3, vec, color='gray', label='raw data (50kHz)')
ax[0].plot(np.arange(len(vec))/old_facq/1e3, vec, color='gray', label='raw data (50kHz)')
    
def filter(vec, tau=5e-3, dt=1e-3/new_facq):
    n_points = int(3.*tau/dt)
    decay_exp = np.exp(-dt/tau*np.arange(n_points))
    decay_exp /= decay_exp.mean()
    return np.array([np.mean(vec[i-n_points:i]*decay_exp) if i>n_points else vec[i] for i in range(len(vec))])

t_new = np.arange(len(dwnsample_data))*1e-3/new_facq
filtered = filter(dwnsample_data, tau=1e-3/new_facq)

def temporal_var(vec, tau=5e-3, dt=1e-3/new_facq):
    n_points = 10 #int(3.*tau/dt)
    decay_exp = np.ones(n_points)
    decay_exp /= decay_exp.mean()
    return np.array([np.std(vec[i-n_points:i]*decay_exp) if i>n_points else 0 for i in range(len(vec))])

variations = temporal_var(filtered, tau=1e-3/new_facq)
                                                                                                       
def get_state_timings(t, vec, threshold=0., tau=TAU):
    i_upward  = np.argwhere((vec[1:]>threshold) & (vec[:-1]<=threshold)) # upward crossings
    i_downward  = np.argwhere((vec[1:]<threshold) & (vec[:-1]>=threshold)) # downward crossings
    t_upward, t_downward  = t[i_upward], t[i_downward]
    DOWN_STATE, UP_STATE = [], []
    iupward, idownward = 0, 0
    if t_upward[0]<t_downward[0]:
        DOWN_STATE.append([0,t_upward[0][0]])
        iupward +=1
    else:
        UP_STATE.append([0,t_downward[0][0]])
        idownward +=1
    while((iupward<len(i_upward)) and (idownward<len(i_downward))):
        DOWN_STATE.append([t_downward[idownward][0],t_upward[iupward][0]])
        UP_STATE.append([t_upward[iupward][0],t_downward[idownward][0]])
        idownward +=1
        iupward +=1
    return DOWN_STATE, UP_STATE

# DOWN_STATE, UP_STATE = get_state_timings(\
#                     1e-3*np.arange(len(dwnsample_data))/new_facq,\
#                         filter(dwnsample_data, tau=TAU), threshold=threshold)

ax[0].plot(t_new, filtered, 'r-',\
        label=str(int(1e3*new_facq))+\
        'Hz subsampling \n +Real-Time Low-Pass, $\\tau$='+\
           str(int(1/new_facq))+'ms', lw=1)
ax[1].plot(t_new, temporal_var(filtered), 'k-', lw=1)
# for dwn in DOWN_STATE:
#     ax[0].fill_between([dwn[0], dwn[1]],\
#                     [vec.min(), vec.min()], [-30, -30],\
#                     color='k', alpha=.1, lw=0)
ax[0].legend(frameon=False, prop={'size':'xx-small'})
set_plot(ax[0], xlabel='time (s)', ylabel='LFP ($\mu$V)')
set_plot(ax[1], xlabel='time (s)', ylabel='$\sigma_{LFP}$ ($\mu$V)')
fig.savefig('fig.png')
